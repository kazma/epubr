FROM nginx:stable AS deployment

COPY nginx.conf /etc/nginx/nginx.conf

WORKDIR /app

COPY *.html .
COPY *.js .

EXPOSE 80